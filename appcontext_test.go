package appcontext

import (
	"errors"
	"testing"
)

func TestErr(t *testing.T) {
	ctx := New()

	// System error
	err := errors.New("Error during processing")

	// Our error system
	ctx.Log().AddError(err).Error("Something broke")
}
