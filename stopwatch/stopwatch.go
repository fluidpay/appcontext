package stopwatch

import (
	"errors"
	"sync"
	"time"
)

// StopWatch is a time tracker extension to track the time in bound of context scope
// timer is the main timer
// points is a concurrent-safe map to store the points
type StopWatch struct {
	timer  time.Time
	points *sync.Map
}

// Start creates a new instance of the tracker
func Start() *StopWatch {
	return &StopWatch{
		timer:  time.Now(),
		points: &sync.Map{},
	}
}

// SinceStart returns the time duration since the tracker initiated
func (t *StopWatch) SinceStart() time.Duration {
	return time.Since(t.timer)
}

// AddPoint add custom point on the timeline
func (t *StopWatch) AddPoint(name string) {
	t.points.Store(name, time.Now())
}

// SincePoint returns the time duration since the point initiated
func (t *StopWatch) SincePoint(name string) (time.Duration, error) {
	value, ok := t.points.Load(name)
	if ok {
		if dur, valid := value.(time.Time); valid {
			return time.Since(dur), nil
		}
		return 0, errors.New("point invalid")
	}
	return 0, errors.New("point not found")
}
