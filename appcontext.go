package appcontext

import (
	"context"

	"bitbucket.org/fluidpay/appcontext/loggit"
)

// AppContext is the primary struct containing usage parameters
type AppContext struct {
	Loggit *loggit.Loggit

	Context context.Context
}

// New returns a pointer to the new AppContext struct
func New() *AppContext {
	return &AppContext{
		Loggit: nil,

		Context: context.Background(),
	}
}

// Log will return a pointer to your loggit and if it hasnt been initiated yet start it
func (ac *AppContext) Log() *loggit.Loggit {
	if ac.Loggit == nil {
		ac.Loggit = loggit.New()
	}
	return ac.Loggit
}

// StartWatch will initiate a new stopwatch if not done already
// func (ac *AppContext) StartWatch() *stopwatch.StopWatch {
// 	if ac.StopWatch == nil {
// 		ac.StopWatch = stopwatch.Start()
// 	}

// 	return ac.StopWatch
// }
