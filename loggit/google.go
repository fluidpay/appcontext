package loggit

import (
	"context"

	"cloud.google.com/go/logging"
)

var googleProjectID = ""
var googleLogName = "api"
var googleClient *logging.Client
var googleLoggerClient *logging.Logger

// SetGoogleProjectID your Google Cloud Platform project ID.
func SetGoogleProjectID(projectID string) {
	googleProjectID = projectID
}

// SetGoogleLogName will set the name of the log to write to
func SetGoogleLogName(name string) {
	googleLogName = name
}

func CloseGoogleClient() {
	googleClient.Close()
}

// outputGoogle will output to tcp connection
func (loggit *Loggit) outputGoogle(level uint8) error {
	// Make sure we have a set google client
	if googleClient == nil || googleLoggerClient == nil {
		ctx := context.Background()

		// Creates a client.
		var err error
		googleClient, err = logging.NewClient(ctx, googleProjectID)
		if err != nil {
			return err
		}

		googleLoggerClient = googleClient.Logger(googleLogName)
	}

	var Severity logging.Severity
	switch level {
	case 4:
		Severity = logging.Critical
	case 3:
		Severity = logging.Error
	case 2:
		Severity = logging.Warning
	case 1:
		Severity = logging.Info
	case 0:
		Severity = logging.Debug
	}

	// Create payload
	payload := make(map[string]interface{})

	// Add Tags
	loggit.Tags.Range(func(key, value interface{}) bool {
		if v, ok := key.(string); ok {
			payload[v] = value
		}
		return true
	})

	// Add Fields
	loggit.Fields.Range(func(key, value interface{}) bool {
		if v, ok := key.(string); ok {
			payload[v] = value
		}
		return true
	})

	// Adds an entry to the log buffer.
	googleLoggerClient.Log(logging.Entry{
		Severity: Severity,
		Payload:  payload,
	})

	return nil
}
