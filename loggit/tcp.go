package loggit

import (
	"fmt"
	"net"
)

var tcpConnection net.Conn

// SetTCP takes in a list of parameters to set syslog Dial
func SetTCP(host, port string) error {
	var err error
	tcpConnection, err = net.Dial("tcp", host+":"+port)
	return err
}

// outputTCP will output to tcp connection
func (loggit *Loggit) outputTCP() error {
	// Double check connection is set
	if tcpConnection == nil {
		panic("OutputTCP must be set before you can use tcp")
	}

	str, err := loggit.String()
	if err != nil {
		return err
	}

	fmt.Fprintf(tcpConnection, str+"\n")

	return nil
}
