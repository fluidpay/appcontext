package loggit

import (
	"fmt"
)

// outputTerminal will take the loggit struct and build to output to terminal
func (loggit *Loggit) outputTerminal() {
	str, err := loggit.String()
	if err != nil {
		fmt.Println(err)
		return
	}

	fmt.Println(str)
}
