package loggit

import (
	"errors"
	"testing"
	"time"

	"github.com/brianvoe/gofakeit/v6"
)

func TestGoogle(t *testing.T) {
	SetOutput("googlelogging")
	SetGoogleProjectID("fair-portal-332320")

	l := New()
	l.AddField("name", gofakeit.FirstName())
	l.Warn("Error info here for TestGoogle")

	time.Sleep(time.Second * 3)

	googleLoggerClient.Flush()
	googleClient.Close()

	time.Sleep(time.Second * 3)
}

func TestGoogleSeverityLevels(t *testing.T) {
	SetOutput("googlelogging")
	SetGoogleProjectID("fair-portal-332320")

	New().Debug("Debug info")
	New().Info("Info info")
	New().Warn("Warn info")
	New().Error("Error info")
	New().Fatal("Fatal info")

	time.Sleep(time.Second * 3)

	googleLoggerClient.Flush()
	googleClient.Close()

	time.Sleep(time.Second * 3)
}

func TestGoogleAddError(t *testing.T) {
	SetOutput("googlelogging")
	SetGoogleProjectID("fair-portal-332320")

	l := New()
	l.AddField("name", gofakeit.FirstName())
	l.AddError(errors.New("Problem happen opening with file"))
	l.Error("Error info here for TestGoogleAddError")

	time.Sleep(time.Second * 3)

	googleLoggerClient.Flush()
	googleClient.Close()

	time.Sleep(time.Second * 3)
}

func BenchmarkGoogleOutput(b *testing.B) {
	SetOutput("googlelogging")
	SetGoogleProjectID("fair-portal-332320")

	l := New()
	l.AddField("name", gofakeit.FirstName())
	l.AddError(errors.New("Benchmark"))
	l.Error("Here you go")

	b.ResetTimer()

	for i := 0; i < b.N; i++ {
		l := New()
		l.AddField("name", gofakeit.FirstName())
		l.AddError(errors.New("BenchmarkFinal"))
		l.Error("Here you go2")
	}

	time.Sleep(time.Second * 3)

	googleLoggerClient.Flush()
	googleClient.Close()

	time.Sleep(time.Second * 3)
}
