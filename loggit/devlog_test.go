package loggit

import (
	"testing"

	"github.com/brianvoe/gofakeit/v6"
)

func TestDevlog(t *testing.T) {
	l := New()
	SetOutput("devlog")

	l.AddTag("info", gofakeit.FirstName())
	l.Info("Info sent to devlog")

	l.AddTag("warn", gofakeit.FirstName())
	l.Warn("Warning sent to devlog")

	l.AddTag("error", gofakeit.FirstName())
	l.Error("Error sent to devlog")
}
