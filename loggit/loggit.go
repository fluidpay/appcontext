package loggit

import (
	"encoding/json"
	"errors"
	"fmt"
	"sync"
	"time"

	"github.com/brianvoe/uhoh"
)

const (
	Debug = "debug"
	Info  = "info"
	Warn  = "warn"
	Error = "error"
	Fatal = "fatal"
)

// outputTypes sets where to output logs to - unless overwritten by settings
var outputTypes = []string{"terminal", "googlelogging", "html", "syslog", "tcp", "devlog"}

// levelTypes sets the numerical value of the level string
var levelTypes = map[uint8]string{0: Debug, 1: Info, 2: Warn, 3: Error, 4: Fatal}

// typeLevels is the reverse of levelType
var typeLevels = map[string]uint8{Debug: 0, Info: 1, Warn: 2, Error: 3, Fatal: 4}

// outputType globally sets the output location
var outputType = outputTypes[0]

// minLevel is the minimal level needed to output
var minLevel uint8 = 0

// Fields is a reusable map string interface type
type Fields map[string]interface{}

// Tags are a map string interface that allow you to add values but get cleared once a severity is called
type Tags map[string]interface{}

// Loggit is the primary struct containing the pieces of info needed
type Loggit struct {
	// Contains the more advanced error with stacktrace usage
	UhOh *uhoh.Err

	// Contains all the fields set by the user
	Fields *sync.Map

	// Tags get cleared upon severity call
	Tags *sync.Map
}

// New will return a new Loggit struct
func New() *Loggit {
	return &Loggit{
		UhOh:   nil,
		Fields: &sync.Map{},
		Tags:   &sync.Map{},
	}
}

// SetOutput will set the output type
func SetOutput(out string) {
	// Check to make sure its within the array types
	for _, t := range outputTypes {
		if t == out {
			outputType = out
			return
		}
	}
}

// SetMinimalOutputLevel will override the default level needed to hit in order to output
func SetMinimalOutputLevel(min string) {
	for i, val := range levelTypes {
		if val == min {
			minLevel = i
			return
		}
	}
}

// AddField will append/overwrite a single map value
func (loggit *Loggit) AddField(k string, v interface{}) *Loggit {
	loggit.Fields.Store(k, v)
	return loggit
}

// AddFields will append/overwrite fields in loggit struct
func (loggit *Loggit) AddFields(fields Fields) *Loggit {
	for k, v := range fields {
		loggit.AddField(k, v)
	}
	return loggit
}

// AddTag will append/overwrite a single map value
func (loggit *Loggit) AddTag(k string, v interface{}) *Loggit {
	loggit.Tags.Store(k, v)
	return loggit
}

// AddTags will append/overwrite tags in loggit struct
func (loggit *Loggit) AddTags(tags Tags) *Loggit {
	for k, v := range tags {
		loggit.AddTag(k, v)
	}
	return loggit
}

// AddError takes in an error struct and attaches "error" to Tags
func (loggit *Loggit) AddError(err error) *Loggit {
	// Check if err is of type uhoh.Err
	var uhohErr *uhoh.Err
	if errors.As(err, &uhohErr) {
		// If it is, copy to new uhoh, then set it to loggit.UhOh
		derefUhohErr := *uhohErr
		loggit.UhOh = &derefUhohErr
	} else {
		// Check if err is wrapped
		if unwrapped := errors.Unwrap(err); unwrapped != nil {
			loggit.UhOh = uhoh.NewStackLevel(unwrapped, 2).SetDescribe(err)
		} else {
			loggit.UhOh = uhoh.NewStackLevel(err, 2)
		}
	}

	return loggit
}

// Log will use the current Loggit struct info and run outputs for it
func (loggit *Loggit) log(level uint8, msg string) {
	// Identify whether or not you want to log based upon loglevel
	if level < minLevel {
		return
	}

	// Set uhoh describe
	if loggit.UhOh != nil {
		// Make sure an empty message doesnt override whats already there
		if msg != "" {
			loggit.UhOh.SetDescribe(errors.New(msg))
		}
	} else {
		loggit.Tags.Store("description", msg)
	}

	// Add Uhoh to tags
	if loggit.UhOh != nil {
		if loggit.UhOh.Original != nil {
			loggit.Tags.Store("error", loggit.UhOh.Original.Error())
		}
		if loggit.UhOh.Describe != nil {
			loggit.Tags.Store("description", loggit.UhOh.Describe.Error())
		}

		loggit.UhOh.SetDate(time.Now().UTC())
	}

	// Set date time from current time
	loggit.Tags.Store("datetime", time.Now().UTC().Format(time.RFC3339))

	switch outputType {
	case "googlelogging":
		loggit.outputGoogle(level)
	case "terminal":
		loggit.outputTerminal()
	case "html":
		loggit.outputHTML()
	case "syslog":
		loggit.outputSyslog(level)
	case "tcp":
		loggit.outputTCP()
	case "devlog":
		loggit.outputDevlog(levelTypes[level])
	case "fake":
		loggit.outputFake()
	}

	// Clear out tags
	loggit.Tags = &sync.Map{}

	// Clear out uhoh(errors)
	loggit.UhOh = nil
}

// Print is just an alias to the Info method
func (loggit *Loggit) Print(msg string) {
	loggit.Info(msg)
}

// Printf is a fmt.Sprintf wrapper around Print
func (loggit *Loggit) Printf(format string, a ...interface{}) {
	loggit.Info(fmt.Sprintf(format, a...))
}

// Debug set the proper level and runs Log
func (loggit *Loggit) Debug(msg string) {
	loggit.log(0, msg)
}

// Debugf is a fmt.Sprintf wrapper around Debug
func (loggit *Loggit) Debugf(format string, a ...interface{}) {
	loggit.Debug(fmt.Sprintf(format, a...))
}

// Info set the proper level and runs Log
func (loggit *Loggit) Info(msg string) {
	loggit.log(1, msg)
}

// Infof is a fmt.Sprintf wrapper around Info
func (loggit *Loggit) Infof(format string, a ...interface{}) {
	loggit.Info(fmt.Sprintf(format, a...))
}

// Warn set the proper level and runs Log
func (loggit *Loggit) Warn(msg string) {
	loggit.log(2, msg)
}

// Warnf is a fmt.Sprintf wrapper around Warn
func (loggit *Loggit) Warnf(format string, a ...interface{}) {
	loggit.Warn(fmt.Sprintf(format, a...))
}

// Warningf is a fmt.Sprintf wrapper around Warn
func (loggit *Loggit) Warningf(format string, a ...interface{}) {
	loggit.Warn(fmt.Sprintf(format, a...))
}

// Error set the proper level and runs Log
func (loggit *Loggit) Error(msg string) {
	loggit.log(3, msg)
}

// Errorf is a fmt.Sprintf wrapper around Error
func (loggit *Loggit) Errorf(format string, a ...interface{}) {
	loggit.Error(fmt.Sprintf(format, a...))
}

// Fatal set the proper level and runs Log
func (loggit *Loggit) Fatal(msg string) {
	loggit.log(4, msg)
}

// Fatalf is a fmt.Sprintf wrapper around Fatal
func (loggit *Loggit) Fatalf(format string, a ...interface{}) {
	loggit.Fatal(fmt.Sprintf(format, a...))
}

// String returns the string representation from the reader and based upon the formatter
func (loggit *Loggit) String() (string, error) {
	outputMap := make(map[string]interface{})

	// Add Tags
	loggit.Tags.Range(func(key, value interface{}) bool {
		if v, ok := key.(string); ok {
			outputMap[v] = value
		}
		return true
	})

	// Add Fields
	loggit.Fields.Range(func(key, value interface{}) bool {
		if v, ok := key.(string); ok {
			outputMap[v] = value
		}
		return true
	})

	// Add Uhoh
	if loggit.UhOh != nil {
		outputMap["error"] = loggit.UhOh.ToMapStr()
	}

	serialized, err := json.Marshal(outputMap)
	if err != nil {
		return "", err
	}

	return string(serialized), nil
}
