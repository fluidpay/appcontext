package loggit

import (
	"testing"

	"github.com/brianvoe/gofakeit/v6"
)

func TestTerminal(t *testing.T) {
	SetOutput("terminal")

	l := New()
	l.AddField("name", gofakeit.FirstName()).Print("Sup")
}
