package loggit

import (
	"bytes"
	"encoding/json"
	"net/http"
)

var devlogPort = "8888"

func SetDevlogPort(port string) {
	devlogPort = port
}

// outputDevlog will output to http connection
func (loggit *Loggit) outputDevlog(level string) error {
	str, err := loggit.String()
	if err != nil {
		return err
	}

	data := struct {
		Level string `json:"level"`
		Data  string `json:"data"`
	}{
		Level: level,
		Data:  str,
	}

	serialized, _ := json.Marshal(data)

	resp, err := http.Post("http://localhost:"+devlogPort+"/adddata", "application/json", bytes.NewBuffer(serialized))
	if err != nil {
		return err
	}
	defer resp.Body.Close()

	return nil
}
