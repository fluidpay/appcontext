package loggit

import (
	"errors"
	"sync"
	"testing"

	"github.com/brianvoe/gofakeit/v6"
	"github.com/brianvoe/uhoh"
)

func init() {
	gofakeit.Seed(0)
}

func TestNew(t *testing.T) {
	l := New()
	if l == nil {
		t.Error("Expected a new logger")
	}
}

func TestAddTag(t *testing.T) {
	l := New()
	l.AddTag("test", "test")
	l.AddTag("test-second", "test-second")
	l.AddTags(Tags{
		"test-third": "test-third",
	})
	l.Info("Info about ...")

	// Check to make sure tags are empty
	val, ok := l.Tags.Load("test")
	if ok {
		t.Errorf("Expected tags to be empty got: %s", val)
	}
}

func TestAddError(t *testing.T) {
	l := New()
	l.AddError(errors.New("Bad thing happen"))
	l.Error("Error message here")
}

func TestPretty(t *testing.T) {
	SetOutput("pretty")

	l := New()
	l.AddFields(Fields{
		"name": gofakeit.FirstName(),
	}).Print("Sup")
}

func TestUhohDereference(t *testing.T) {
	SetOutput("fake")

	newErr := uhoh.NewStr("original error")

	l := New().AddError(newErr)
	l.Error("descriptive error")

	// fmt.Printf("%+v\n", newErr)

	if newErr.Describe != nil {
		t.Error("Should have not been able to update describe error")
	}
}

func TestConcurrentWithFields(t *testing.T) {
	var wg = &sync.WaitGroup{}
	l := New()
	for i := 0; i < 10000; i++ {
		wg.Add(1)
		go func(wg *sync.WaitGroup) {
			l.AddField("test", "test")
			l.AddField("test-second", "test-it")
			wg.Done()
		}(wg)
	}
	l.Info("Info about ...")
	wg.Wait()
}

func BenchmarkConcurrentLogWithFields(b *testing.B) {
	for i := 0; i < b.N; i++ {
		l := New()
		l.AddField("test", "test")
		l.AddField("test-second", "test-it")
		l.Info("Info about ...")
	}
}
