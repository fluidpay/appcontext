package loggit

import (
	"log/syslog"
	"testing"

	"github.com/brianvoe/gofakeit/v6"
)

func TestSyslog(t *testing.T) {
	SetSyslog("", "", syslog.LOG_LOCAL7, "fp-api")
	l := New()
	l.AddField("name", gofakeit.FirstName())
	l.Info("Info sent to syslog")
}
