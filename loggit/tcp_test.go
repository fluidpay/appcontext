package loggit

import (
	"bufio"
	"log"
	"net"
	"testing"

	"github.com/brianvoe/gofakeit/v6"
)

// Start a simple tcp server for testing purposes
func init() {
	ln, err := net.Listen("tcp", "127.0.0.1:9999")
	if err != nil {
		log.Fatal(err)
	}
	go func() {
		for {
			conn, err := ln.Accept()
			if err != nil {
				log.Fatal(err)
			}
			defer conn.Close()

			scanner := bufio.NewScanner(conn)
			for scanner.Scan() {
				message := scanner.Text()
				conn.Write([]byte(message + "\n"))
			}

			if err := scanner.Err(); err != nil {
				log.Fatal(err)
			}
		}
	}()
}

func TestTCP(t *testing.T) {
	SetTCP("127.0.0.1", "9999")
	l := New()
	SetOutput("tcp")
	l.AddField("name", gofakeit.FirstName())
	l.Info("Info sent to tcp")
}

func BenchmarkTCP(b *testing.B) {
	SetTCP("127.0.0.1", "9999")
	for i := 0; i < b.N; i++ {
		l := New()
		SetOutput("tcp")
		l.AddField("name", gofakeit.FirstName())
		l.Info(gofakeit.Sentence(gofakeit.Number(3, 8)))
	}
}
