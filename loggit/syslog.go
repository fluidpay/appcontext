package loggit

import (
	"log/syslog"
)

var syslogWriter *syslog.Writer

// SetSyslog takes in a list of parameters to set syslog Dial
func SetSyslog(network, raddr string, priority syslog.Priority, tag string) error {
	var err error
	syslogWriter, err = syslog.Dial(network, raddr, priority, tag)
	return err
}

// outputSyslog will output to syslog writer based upon loggit struct
func (loggit *Loggit) outputSyslog(level uint8) error {
	// Double check writer is set
	if syslogWriter == nil {
		// Default example
		// SetSyslog("", "", syslog.LOG_LOCAL7, "fp-api")

		// Error that it must be set before you can call OutputSyslog
		panic("OutputSyslog must be set before you can use syslog")
	}

	format, err := loggit.String()
	if err != nil {
		syslogWriter.Crit(err.Error())
		return err
	}

	switch level {
	case 4:
		return syslogWriter.Crit(format)
	case 3:
		return syslogWriter.Err(format)
	case 2:
		return syslogWriter.Warning(format)
	case 1:
		return syslogWriter.Debug(format)
	case 0:
		return syslogWriter.Info(format)
	default:
		return nil
	}
}
