package loggit

var fakeOutput = ""

// outputFake will output to global string to be used for testing
func (loggit *Loggit) outputFake() error {
	format, err := loggit.String()
	if err != nil {
		fakeOutput = err.Error()
	}
	fakeOutput = format

	return nil
}
