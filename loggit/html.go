package loggit

import (
	"encoding/json"
	"fmt"
)

// outputHTML will take the loggit struct and build a formatted html output
func (loggit *Loggit) outputHTML() {
	// Tags
	jsonOut, err := json.MarshalIndent(loggit.Tags, "", "&nbsp;&nbsp;&nbsp;&nbsp;")
	if err != nil {
		fmt.Print(err.Error())
	}
	fmt.Println(string(jsonOut))

	// Fields
	jsonOut, err = json.MarshalIndent(loggit.Fields, "", "&nbsp;&nbsp;&nbsp;&nbsp;")
	if err != nil {
		fmt.Print(err.Error())
	}
	fmt.Println(string(jsonOut))
}
